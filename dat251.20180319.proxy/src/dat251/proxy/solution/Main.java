package dat251.proxy.solution;

public class Main {

	public static void main(String[] args) {
		Image image = new ProxyImage("test_10mb.jpg");

		//image will be loaded from disk
		System.out.println("DISPLAY 1");
		image.display();
		
		System.out.println();

		//image will not be loaded from disk
		System.out.println("DISPLAY 2");
		image.display();
	}

}